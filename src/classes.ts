/*
console.clear();
class Pessoa {
    nome: string;
    idade: number;

    constructor(n: string, i: number) {
        this.nome = n;
        this.idade = i;
    }
}

class Pessoa2 {
    readonly nome: string;
    readonly idade: number;

    constructor(n: string, i: number) {
        this.nome = n;
        this.idade = i;
    }
}

class Pessoa3 {
    constructor(private _nome: string, private _idade: number) { }

    toString(): string {
        return `nome = ${this._nome}, idade = ${this._idade}`;
    }

    getNome(): string {
        return this._nome;
    }

    get idade(): number {
        return this._idade;
    }

    set idade(x: number) {
        this._idade = x;
    }

}

function imprime(p: { nome: string, idade: number }): void {
    console.log(p.nome + ' ' + p.idade);
}

let p3 = new Pessoa3('Clauber', 22);
console.log(p3.getNome());
p3.idade = 30;
console.log(p3.idade);
console.log(p3.toString());

let umaPessoa: Pessoa = new Pessoa('JJ', 19);
let outraPessoa: Pessoa2 = umaPessoa;
imprime(umaPessoa);
imprime(outraPessoa);

class Produto {
    constructor(private _nome: string, private _preco: number) { }

    get nome(): string {
        return this._nome;
    }

    get preco(): number {
        return this._preco;
    }

    set preco(x: number) {
        this._preco = x;
    }

    toString(): string{
        return `nome = ${this._nome} idade = ${this._preco}`;
    }
}

class ProdutoPerecivel extends Produto {
    constructor(_nome: string, _preco: number, private _dataValidade: Date) {
        super(_nome, _preco);
    }
    get dataValidade():string{
        return this._dataValidade.toLocaleDateString();
    }
    toString(): string{
        return super.toString() + ` data = ${this._dataValidade.toLocaleDateString()}`;
    }
}

let prod1 = new Produto('prod1', 9999);
let prod2 = new ProdutoPerecivel('prod2', 100, new Date(2019,10,7));
console.log(prod2.toString());

*/