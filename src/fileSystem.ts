import * as fs from 'fs';

function lerArquivoAsync(fileName: string): Promise<string> {
    return new Promise((res, rej) => {
        try {
            const data = fs.readFileSync(fileName, 'utf-8');
            res(data);
        } catch (e) {
            rej(e);
        }
    });
}

(async () => {
    try {
        let dados = await lerArquivoAsync('dados2.txt');
        console.log(dados);
    } catch (e) {
        console.log(e.message);
    }
})();



/*
try {
    const text = 'maluco';
    fs.writeFileSync('./files/dados.txt', text);
    fs.readFileSync('./files/dados.txt');
} catch (e) {
    console.log(`Erro: ${e.name} - ${e.message}`);
}

try {
    let text = ':^)';
    fs.writeFile('dados2.txt', text, (err): void => {
        if (!err) {
            console.log('Arquivo escrito');
        } else {
            throw err;
        }
    });
    console.log('ツ...')
} catch (e) {
    console.log(`Erro: ${e.name} - ${e.message}`);
}

for (let i = 0; i < 9; i++) {
    fs.writeFileSync('./files/numbers.txt', i);
}

fs.readFile('pessoa.json', 'utf-8', (err, data) => {
    if (err) {
        console.log('erro de leitura de arquivo');
    } else {
        try {
            const obj = JSON.parse(data);
            console.log(obj.nome);
            console.log(obj.idade);
        } catch (e) {
            console.log(`Erro: ${e.name} - Mensagem: ${e.message}`);
        }
    }
});

lerArquivoAsync('dados.txt').then(
    data => console.log(data),
    e => console.log(e)
);

lerArquivoAsync('dados.txt')
    .then(data => console.log(data))
    .catch(e => console.log(e.message));

*/

