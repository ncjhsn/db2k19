import { Document, Schema, model, Model, connect } from 'mongoose';

interface Pessoa {
    name: string,
    idade: number
}

interface PessoaDocument extends Pessoa, Document { }

const pSchema: Schema = new Schema({
    name: { type: String, required: true, minlength: 1, maxlength: 50 },
    idade: { type: Number, require: true, min: 0 }
});

const pModel: Model<PessoaDocument> = model('Pessoa', pSchema, 'Pessoas');

async function main() {
    try {
        const url = 'mongodb://localhost:27017/testeMongoose';
        const client = await connect(url, { useNewUrlParser: true });
        console.log('connected');

        //↓↓↓ INSERT
        /* const doc = await pModel.create({ name: 'Tony Stark', idade: 37 });
        let res = await doc.save();
        console.log(res);
        doc.idade = 38;
        res = await doc.save();
        console.log(res);
        await doc.save(); */

        //CONSULTAR ↓↓↓
        const returnPessoa = pModel.find();
        const res = await returnPessoa.exec();
        res.forEach(p => console.log(p.name));

        //REMOVER ↓↓
        //const removePessoa = pModel.findOneAndDelete({nome: "Tony Stak"}).exec();
        


        if (client && client.connection) {
            client.connection.close();
        }
    } catch (e) {
        throw e.message;
    }
}

main();