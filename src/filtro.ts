console.clear();
/*
class Coin {
    constructor(private _value: number, private _name: string) { }

    get valor(): number {
        return this._value;
    }

    get nome(): string {
        return this._name;
    }
}

class Vault {
    private coins: Coin[] = [];

    add(umaMoeda: Coin): void {
        this.coins.push(umaMoeda);
    }

    total(): number {
        const somador: (x: number, y: Coin) => number = (x, y) => {
            return x + y.valor;
        };

        return this.coins.reduce(somador, 0);
    }

}

abstract class Fig {
    constructor(private _centroX: number, private _centroY: number) { }

    get x(): number {
        return this._centroX;
    }

    get y(): number {
        return this._centroY;
    }

    abstract area(): number;
}

class Circulo extends Fig {
    constructor(x: number, y: number, private _raio: number) {
        super(x, y);
    }

    area(): number {
        return Math.PI * Math.pow(this._raio, 2);
    }

    get raio(): number {
        return this._raio;
    }
}

interface Desenhavel {
    x: number,
    y: number
}

const draw = (d: Desenhavel) => {
    return `x = ${d.x}, y = ${d.y}`;
}

interface Predicado<T> {
    (item: T): boolean;
}

function filtrar<T>(array: T[], filtro: Predicado<T>): T[] {
    let resultados: T[] = [];
    for (let i = 0; i < array.length; i++) {
        if (filtro(array[i])) {
            resultados.push(array[i]);
        }
    }
    return resultados;
}


class Per {
    nome: string;
    idade: number;

    constructor(n: string, i: number) {
        this.nome = n;
        this.idade = i;
    }
}

let umaP = new Pessoa('John', 22);
let json = JSON.stringify(umaP);
console.log(json);
*/