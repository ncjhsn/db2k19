import { MongoClient, Db, Cursor } from 'mongodb';

async function Main() {
    const url = 'mongodb://localhost:27017/teste';
    try {
        const client: MongoClient = await MongoClient.connect(url, { useNewUrlParser: true });
        const db: Db = client.db('teste');

        //inserir ↓↓
        //const p = { nome: 'Stark', idade: 26 }
        //let insertIt = await db.collection('pessoas').insertOne(p);
        //console.log(`Inserido: [${insertIt.insertedCount}]`);

        //buscar ↓↓
        //const index: Cursor = db.collection('pessoas').find();
        //await index.forEach(doc => console.log(doc.nome + ' - ' + doc.idade));

        //update ↓↓
        //const update = await db.collection('pessoas').updateOne({ nome: 'Steve' }, { $set: { nome: 'Stark' } });
        //console.log(`Alterado - ${update.modifiedCount}`);

        //remove ↓↓
        //const remove = await db.collection('pessoas').deleteOne({nome: 'Stark'});
        //console.log(`Removidos: [${remove.deletedCount}]`);

        await client.close();
    } catch (e) {
        throw e.message;
    }
}

Main();