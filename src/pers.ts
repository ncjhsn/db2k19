import * as fs from 'fs';
import { Vault, Coin } from './coinVault'
import { fileURLToPath } from 'url';
import { promises } from 'fs';
const fspromises = fs.promises;

export async function lerCofre(filename: string): Promise<Vault> {
    return fs.promises.readFile(filename, 'utf-8')
        .then(data => JSON.parse(data))
        .then(obj => {
            const cofre = new Vault();
            for (let i = 0; i < obj.coins.length; i++) {
                cofre.add(new Coin(obj.coins[i]._value, obj.coins[i]._name));
            }
            return cofre;
        });
}

export async function escreverCofre(cofre: Vault, filename: string) {
    const json = JSON.stringify(cofre);
    return promises.writeFile(filename, json).then(res => {
        console.log('salvo!!')
    })
        .catch(err => {
            console.log(err.message);
        });
}

let v: Vault = new Vault();
v.add(new Coin(5, '$'));
v.add(new Coin(500, '¥'));
v.add(new Coin(5200, '¥'));
v.add(new Coin(15200, '¥'));
v.add(new Coin(99999, '¥'));

escreverCofre(v, 'cofre.json');

export async function aw(filename: string): Promise<any> {
    const data = await promises.readFile(filename, 'utf-8');
    try {
        const obj = JSON.parse(data);
        const cofre = new Vault();

        for (let i = 0; i < obj.coins.length; i++) {
            cofre.add(new Coin(obj.coins[i]._value, obj.coins._name));
        }

        return cofre;
    } catch (e) {
        throw e.message;
    }
}

async function main() {
    aw('cofre.json')
        .then(cofre => console.log(cofre.total()))
        .catch(err => {
            console.log('Erro de leitura')
            console.log(err.message);
        })
}

main();


































/*
NÃO FAZER ↓↓↓↓↓

export async function salvarCofre(cofre: Vault, filename: string): Promise<void> {
    const json = JSON.stringify(Vault);
    fs.writeFile(filename, json, err => {
        if (err !== null) {
            throw err;
        }
    });
}

export async function lerCofre(filename: string): Promise<void> {
    const json = await fspromises.readFile(filename);
    fs.readFile(filename, 'utf-8', (err, data) => {
        if (err !== null) {
            throw err;
        }
        const o = JSON.parse(data);
        return new Vault();
    });
}

export function lerCofre(filename: string, callback: (err: Error | null, data?: Vault) => void): void {
    fs.readFile(filename, 'utf-8', (err, data) => {
        if (err) {
            callback(err);
        }

        const obj = JSON.parse(data);
        const cofre = new Vault();

        callback(null, cofre);
    });
}
*/