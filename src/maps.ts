/*const area = (r: number) => {
    return Math.PI * r ** 2;
}

const circ = (r: number) => {
    return 2 * Math.PI * r;
}

const createName = (firstName: string, ...lastName: string[]) => {
    return firstName + " " + lastName.join(" ");
}

const x = (x: number, y: number) => x * y;

function y(x: number): (y: number) => number {
    return (numero => numero * x);
}

const showMapLog = (value: string, key: string, map: Map<string, string>) => {
    console.log(`map.get('${key}') = ${value}`);
};

const interval = (x: number, y: number) => {
    for (let i = x; i < y + 1; i++) {
        if (i % 2 === 0) {
            return i;
        }
    }
}

const min = (x: number, y: number) => {
    if (x > y) {
        return 'X maior que Y';
    } else if (y > x) {
        return 'Y maior que X';
    } else {
        return 'iguais';
    }
}

const pot = (x: number, y: number) => {
    if (x > 0 && y > 0) {
        return Math.pow(x, y);
    } else if (y == 0) {
        return 1;
    } else {
        return 'valores > que 0';
    }
}

const strings = (letter: string) => {
    const firstLetter = letter[0].toUpperCase();
    return firstLetter + letter.substr(1);
}

function frequencia(array: number[]): Map<number, number> {
    let contagem = new Map<number, number>();
    for (let x of array) {
        if (!contagem.has(x)) {
            contagem.set(x, 1);
        } else {
            contagem.set(x, contagem.get(x)! + 1);
        }
    }
    return contagem;
}

function frequenciav2(array: number[]): Map<number,number>{
    return array.reduce((contagem, valor) => contagem.set(valor,(contagem.get(valor)||0)+1), new Map<number,number>());
}

let mapa: Map<string, string>;
mapa = new Map();

let a = {};
let b = Object.create(a);
console.log(Object.getPrototypeOf(b) === a);
// alt shift f
*/