import * as mongoose from 'mongoose';
import { connect, model } from 'mongoose';

async function k() {
    try {
        const url = 'mongodb://localhost:27017/testeMongoose';
        const client = await connect(url, { useNewUrlParser: true });

        //Define schema ↓↓
        const pscheme = new mongoose.Schema({
            name: { type: String, required: true, minlength: 1, maxlength: 50 },
            idade: { type: Number, require: true, min: 0 }
        });
        const pmodel = model('Pessoa', pscheme, 'pessoas');

        //Inserir doc ↓↓
        //let doc = new pmodel({ name: 'JJ', idade: 32 });
        //await doc.save();

        //Retornar ↓↓
        //let returnDoc = pmodel.where('idade').lt(30).sort({field: 'asc', test: 1});
        //const index = await returnDoc.exec();
        //console.log(index);

        //Update ↓↓
        //let docPessoa = await pmodel.findById('5d028dfeeb14731bfcecba98').exec();
        //docPessoa!.set('idade', 23);
        //await docPessoa!.save();

        //remove ↓↓
        let removePessoa = await pmodel.findByIdAndDelete('5d0291abf30c5f064c2a143a').exec();
        await removePessoa!.save();

        if (client && client.connection) {
            client.connection.close();
        }

    } catch (e) {
        throw e.message;
    }
}

k();