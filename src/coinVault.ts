export class Coin {
    constructor(private _value: number, private _name: string) { }

    get valor(): number {
        return this._value;
    }

    get nome(): string {
        return this._name;
    }
}

export class Vault {
    private coins: Coin[] = [];

    add(umaMoeda: Coin): void {
        this.coins.push(umaMoeda);
    }

    total(): number {
        const somador: (x: number, y: Coin) => number = (x, y) => {
            return x + y.valor;
        };

        return this.coins.reduce(somador, 0);
    }

}

let c: Coin = new Coin(0, 'R');
let v: Vault = new Vault();
v.add(new Coin(.1, "C"));
v.add(new Coin(0, "R"));
v.add(new Coin(99, "R"));

