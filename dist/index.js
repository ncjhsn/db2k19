"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongodb_1 = require("mongodb");
function Main() {
    return __awaiter(this, void 0, void 0, function* () {
        const url = 'mongodb://localhost:27017/teste';
        try {
            const client = yield mongodb_1.MongoClient.connect(url, { useNewUrlParser: true });
            const db = client.db('teste');
            //inserir ↓↓
            //const p = { nome: 'Stark', idade: 26 }
            //let insertIt = await db.collection('pessoas').insertOne(p);
            //console.log(`Inserido: [${insertIt.insertedCount}]`);
            //buscar ↓↓
            //const index: Cursor = db.collection('pessoas').find();
            //await index.forEach(doc => console.log(doc.nome + ' - ' + doc.idade));
            //update ↓↓
            //const update = await db.collection('pessoas').updateOne({ nome: 'Steve' }, { $set: { nome: 'Stark' } });
            //console.log(`Alterado - ${update.modifiedCount}`);
            //remove ↓↓
            //const remove = await db.collection('pessoas').deleteOne({nome: 'Stark'});
            //console.log(`Removidos: [${remove.deletedCount}]`);
            yield client.close();
        }
        catch (e) {
            throw e.message;
        }
    });
}
Main();
//# sourceMappingURL=index.js.map